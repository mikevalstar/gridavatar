import './style.css';

import gridavatar from './src/gridavatar.js';

const img1 = gridavatar('mike@valstar.dev132143', { scatter: 0 });
const img2 = gridavatar('sdfgsdfgdsfgsdfgfgsd', { scatter: 0 });
const img3 = gridavatar(4365324554322345, { scatter: 0 });
const img4 = gridavatar(4235443554325, { scatter: 0 });
const img5 = gridavatar(2546352665662662, { scatter: 0 });
const img6 = gridavatar('viuvlbsvjlkbhdsjkl', { scatter: 0 });
const img7 = gridavatar('mike@v13214', { scatter: 0 });
const img8 = gridavatar('lasdfgjhblgvikb', { scatter: 0 });
const img9 = gridavatar('lwerkjbltkb', { scatter: 0 });
const img10 = gridavatar('lkjegblkjbgsdf', { scatter: 0 });
const img11 = gridavatar('sdf.;kjngklnsdf', { scatter: 0 });
const img12 = gridavatar('s;dkjglkjsdfbhglkjh', { scatter: 0 });
const img13 = gridavatar('fsdgdvxfc', { scatter: 0 });
const img14 = gridavatar(24365567567, { scatter: 0 });
const img15 = gridavatar('mike@valstar.dev132143', { scatter: 0 });
const img16 = gridavatar('mike@valstar.dev', {
  height: 128,
  width: 128,
  luminosity: 'light',
  type: 'square',
  text: 'MV',
});
img16.download = 'mvavatar.png';

document.querySelector('#avatar').appendChild(img1);
document.querySelector('#avatar').appendChild(img2);
document.querySelector('#avatar').appendChild(img3);
document.querySelector('#avatar').appendChild(img4);
document.querySelector('#avatar').appendChild(img5);
document.querySelector('#avatar').appendChild(img6);
document.querySelector('#avatar').appendChild(img7);
document.querySelector('#avatar').appendChild(img8);
document.querySelector('#avatar').appendChild(img9);
document.querySelector('#avatar').appendChild(img10);
document.querySelector('#avatar').appendChild(img11);
document.querySelector('#avatar').appendChild(img12);
document.querySelector('#avatar').appendChild(img13);
document.querySelector('#avatar').appendChild(img14);
document.querySelector('#avatar').appendChild(img15);
document.querySelector('#avatar').appendChild(img16);
