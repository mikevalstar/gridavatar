# Grid Avatar

A small (8.5kb, 2.9kb gzipped) script for generating attractive random but predictable avatars using the canvas element. 
Letting you have your own avatars instead of relying on applications like Gravatar which track your users.

The images generated are procedurally generated and therefor deterministic based on the seed and will always generate the same.

## Demo
![Demo](demo.png)
### [Demo Website](https://gridavatar.mikevalstar.com/)

## Usage
To use it `yarn add @mikevalstar/gridavatar`

```javascript
import gridavatar from '@mikevalstar/gridavatar';

var avatar = gridavatar('seed text');
var div = document.querySelector('#avatar-spot');
div.appendChild(avatar); // place it in the DOM
```
For use with React:
```jsx
import gridavatar from "@mikevalstar/gridavatar";

export default ({className, seed}) => {
  return <img class={className} download='gridavatar.png' src={gridavatar(seed, {height: 128, width: 128, output: 'dataURL'})} />;
};
```

## Libraries
Although this project does not rely on any libraries in the package.json, some code was cloned from:
- [randomColor](https://github.com/davidmerfield/randomColor) - Modified to work in parallel (moved seed to a local scope)
- [Mersenne Twister implementation](https://gist.github.com/banksean/300494) - Also modified to work inside a local scope

## Options

`height` - height in pixels [**default:** `256`]

`width` - width in pixels [**default:** `256`]

`type` - Which of the avatar styles to use: `circle`, `square`, `hex` [**default:** ['circle', 'square', 'hex']]

`objSize` - Size of the objects in the grid: [**default:** width / 16 (max: 14)]

`scatter` - how much to scatter the shapes [**default:** `0`]

`luminosity` - See [randomColor](https://github.com/davidmerfield/randomColor) - `random`, `bright`, `light` or `dark` [**default:** ['bright', 'light', 'dark']]

`text` - The text to display (centered on image) [**default:** false]

`textSize` - Font Size [**default:** 64]

`output` = Format you would like the output: `image`, `dataURL`, `canvas` [**default:** `image`]

## Examples

```javascript
// returns a 128x128 Image element with a square tile background and the text "MV" overlaying it
gridavatar('mike@valstar.dev', {
  height: 128,
  width: 128,
  luminosity: ['light'],
  type: ['square'],
  text: 'MV'
})

// returns a 32x32 Image element with a background pattern and no text. 
// this will be very similar to the previous one due to having the same seed. but not exactly the same.
gridavatar('mike@valstar.dev', {
  height: 32,
  width: 32,
  text: false
})
```