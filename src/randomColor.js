// Shared color dictionary
const colorDictionary = {};

// Populate the color dictionary
loadColorBounds();

const randomColor = (options) => {
  // randomColor by David Merfield under the CC0 license
  // https://github.com/davidmerfield/randomColor/
  // Modified to isolate the random number generation to a single call

  // Random number generator
  const rand = options.rand;

  // check if a range is taken
  const colorRanges = [];

  function randomColorInit (options) {
    options = options || {};

    // Check if we need to generate multiple colors
    if (options.count !== null && options.count !== undefined) {
      const totalColors = options.count;
      const colors = [];
      // Value false at index i means the range i is not taken yet.
      for (let i = 0; i < options.count; i++) {
        colorRanges.push(false);
      }
      options.count = null;

      while (totalColors > colors.length) {
        const color = randomColorInit(options);

        colors.push(color);
      }

      return colors;
    }

    // First we pick a hue (H)
    const H = pickHue(options);
    // Then use H to determine saturation (S)
    const S = pickSaturation(H, options);
    const B = pickBrightness(H, S, options);

    // Then we return the HSB color in the desired format
    return HSVtoHSL([H, S, B]);
  };

  function pickHue (options) {
    let hueRange = getRealHueRange(options.hue);

    let hue = randomWithin(hueRange);

    // Each of colorRanges.length ranges has a length equal approximatelly one step
    const step = (hueRange[1] - hueRange[0]) / colorRanges.length;

    let j = parseInt((hue - hueRange[0]) / step);

    // Check if the range j is taken
    if (colorRanges[j] === true) {
      j = (j + 2) % colorRanges.length;
    } else {
      colorRanges[j] = true;
    }

    const min = (hueRange[0] + j * step) % 359;
    const max = (hueRange[0] + (j + 1) * step) % 359;

    hueRange = [min, max];

    hue = randomWithin(hueRange);

    if (hue < 0) { hue = 360 + hue; }
    return hue;
  }

  function pickSaturation (hue, options) {
    if (options.luminosity === 'random') {
      return randomWithin([0, 100]);
    }

    const saturationRange = getSaturationRange(hue);

    let sMin = saturationRange[0];
    let sMax = saturationRange[1];

    switch (options.luminosity) {
      case 'bright':
        sMin = 55;
        break;

      case 'dark':
        sMin = sMax - 10;
        break;

      case 'light':
        sMax = 55;
        break;
    }

    return randomWithin([sMin, sMax]);
  }

  function pickBrightness (H, S, options) {
    let bMin = getMinimumBrightness(H, S);
    let bMax = 100;

    switch (options.luminosity) {
      case 'dark':
        bMax = bMin + 20;
        break;

      case 'light':
        bMin = (bMax + bMin) / 2;
        break;

      case 'random':
        bMin = 0;
        bMax = 100;
        break;
    }

    return randomWithin([bMin, bMax]);
  }

  function getMinimumBrightness (H, S) {
    const lowerBounds = getColorInfo(H).lowerBounds;

    for (let i = 0; i < lowerBounds.length - 1; i++) {
      const s1 = lowerBounds[i][0];
      const v1 = lowerBounds[i][1];

      const s2 = lowerBounds[i + 1][0];
      const v2 = lowerBounds[i + 1][1];

      if (S >= s1 && S <= s2) {
        const m = (v2 - v1) / (s2 - s1);
        const b = v1 - m * s1;

        return m * S + b;
      }
    }

    return 0;
  }

  function getSaturationRange (hue) {
    return getColorInfo(hue).saturationRange;
  }

  function getColorInfo (hue) {
    // Maps red colors to make picking hue easier
    if (hue >= 334 && hue <= 360) {
      hue -= 360;
    }

    for (const colorName in colorDictionary) {
      const color = colorDictionary[colorName];
      if (color.hueRange &&
          hue >= color.hueRange[0] &&
          hue <= color.hueRange[1]) {
        return colorDictionary[colorName];
      }
    } return 'Color not found';
  }

  function randomWithin (range) {
    return rand(range[0], range[1]);
  }

  function HexToHSB (hex) {
    hex = hex.replace(/^#/, '');
    hex = hex.length === 3 ? hex.replace(/(.)/g, '$1$1') : hex;

    const red = parseInt(hex.substr(0, 2), 16) / 255;
    const green = parseInt(hex.substr(2, 2), 16) / 255;
    const blue = parseInt(hex.substr(4, 2), 16) / 255;

    const cMax = Math.max(red, green, blue);
    const delta = cMax - Math.min(red, green, blue);
    const saturation = cMax ? (delta / cMax) : 0;

    switch (cMax) {
      case red: return [60 * (((green - blue) / delta) % 6) || 0, saturation, cMax];
      case green: return [60 * (((blue - red) / delta) + 2) || 0, saturation, cMax];
      case blue: return [60 * (((red - green) / delta) + 4) || 0, saturation, cMax];
    }
  }

  function HSVtoHSL (hsv) {
    const h = hsv[0];
    const s = hsv[1] / 100;
    const v = hsv[2] / 100;
    const k = (2 - s) * v;

    return [
      h,
      Math.round(s * v / (k < 1 ? k : 2 - k) * 10000) / 100,
      k / 2 * 100,
    ];
  }

  // get The range of given hue when options.count!=0
  function getRealHueRange (colorHue) {
    if (!isNaN(colorHue)) {
      const number = parseInt(colorHue);

      if (number < 360 && number > 0) {
        return getColorInfo(colorHue).hueRange;
      }
    } else if (typeof colorHue === 'string') {
      if (colorDictionary[colorHue]) {
        const color = colorDictionary[colorHue];

        if (color.hueRange) {
          return color.hueRange;
        }
      } else if (colorHue.match(/^#?([0-9A-F]{3}|[0-9A-F]{6})$/i)) {
        const hue = HexToHSB(colorHue)[0];
        return getColorInfo(hue).hueRange;
      }
    }
  }

  return randomColorInit(options);
};

export default randomColor;

function defineColor (name, hueRange, lowerBounds) {
  const sMin = lowerBounds[0][0];
  const sMax = lowerBounds[lowerBounds.length - 1][0];

  const bMin = lowerBounds[lowerBounds.length - 1][1];
  const bMax = lowerBounds[0][1];

  colorDictionary[name] = {
    hueRange,
    lowerBounds,
    saturationRange: [sMin, sMax],
    brightnessRange: [bMin, bMax],
  };
}

function loadColorBounds () {
  defineColor(
    'monochrome',
    null,
    [[0, 0], [100, 0]],
  );

  defineColor(
    'red',
    [-26, 18],
    [[20, 100], [30, 92], [40, 89], [50, 85], [60, 78], [70, 70], [80, 60], [90, 55], [100, 50]],
  );

  defineColor(
    'orange',
    [18, 46],
    [[20, 100], [30, 93], [40, 88], [50, 86], [60, 85], [70, 70], [100, 70]],
  );

  defineColor(
    'yellow',
    [46, 62],
    [[25, 100], [40, 94], [50, 89], [60, 86], [70, 84], [80, 82], [90, 80], [100, 75]],
  );

  defineColor(
    'green',
    [62, 178],
    [[30, 100], [40, 90], [50, 85], [60, 81], [70, 74], [80, 64], [90, 50], [100, 40]],
  );

  defineColor(
    'blue',
    [178, 257],
    [[20, 100], [30, 86], [40, 80], [50, 74], [60, 60], [70, 52], [80, 44], [90, 39], [100, 35]],
  );

  defineColor(
    'purple',
    [257, 282],
    [[20, 100], [30, 87], [40, 79], [50, 70], [60, 65], [70, 59], [80, 52], [90, 45], [100, 42]],
  );

  defineColor(
    'pink',
    [282, 334],
    [[20, 100], [30, 90], [40, 86], [60, 84], [80, 80], [90, 75], [100, 73]],
  );
}
