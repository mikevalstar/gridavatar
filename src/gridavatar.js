import randomColor from './randomColor';
import randomGenInit, { cyrb128 } from './mullberry32';

const getHSLStyle = (hsl) => `hsl(${hsl[0]}, ${hsl[1]}%, ${hsl[2]}%)`;

const hslToRgb = (h, s, l) => {
  l /= 100;
  const a = s * Math.min(l, 1 - l) / 100;
  const f = n => {
    const k = (n + h / 30) % 12;
    const color = l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
    return Math.round(255 * color); // convert to Hex and prefix "0" if needed
  };
  return [f(0), f(8), f(4)];
};

const getContrastColor = (hsl) => {
  const rgb = hslToRgb(hsl[0], hsl[1], hsl[2]);
  console.log(rgb);
  const luma = (0.2126 * rgb[0]) + (0.7152 * rgb[1]) + (0.0722 * rgb[2]);
  return (luma >= 165) ? '#222' : '#fff';
};

const drawHexBoard = (ctx, similarColors, width, height, sideLength, pixelVariance, getRandomSizeVariance, randomArrayItem) => {
  let i, j, x, y;

  const hexagonAngle = 0.523598776; // 30 degrees in radians
  const offset = getRandomSizeVariance(sideLength * 1.3, 0);
  const hexHeight = Math.sin(hexagonAngle) * sideLength;
  const hexRadius = Math.cos(hexagonAngle) * sideLength;
  const hexRectangleHeight = sideLength + 2 * hexHeight;
  const hexRectangleWidth = 2 * hexRadius;

  for (i = 0; i < width / sideLength + 1; ++i) {
    for (j = 0; j < height / sideLength + 1; ++j) {
      x = i * hexRectangleWidth + ((j % 2) * hexRadius) - offset + getRandomSizeVariance(pixelVariance, 0.5);
      y = j * (sideLength + hexHeight) - offset + getRandomSizeVariance(pixelVariance, 0.5);
      ctx.beginPath();
      ctx.moveTo(x + hexRadius, y);
      ctx.lineTo(x + hexRectangleWidth, y + hexHeight);
      ctx.lineTo(x + hexRectangleWidth, y + hexHeight + sideLength);
      ctx.lineTo(x + hexRadius, y + hexRectangleHeight);
      ctx.lineTo(x, y + sideLength + hexHeight);
      ctx.lineTo(x, y + hexHeight);
      ctx.closePath();
      const newHSL = randomArrayItem(similarColors);
      ctx.fillStyle = getHSLStyle(newHSL);
      ctx.fill();
    }
  }
};

const drawSquareBoard = (ctx, similarColors, width, height, sideLength, pixelVariance, getRandomSizeVariance, randomArrayItem) => {
  let i, j, x, y;

  sideLength *= 1.75;
  // var shiftValue = sideLength * 0.25
  const offset = getRandomSizeVariance(sideLength * 1.3, 0);

  for (i = 0; i < width / sideLength + 1; ++i) {
    for (j = 0; j < height / sideLength + 1; ++j) {
      x = i * sideLength - offset + getRandomSizeVariance(pixelVariance, 0.5);// + shiftValue
      y = j * sideLength - offset + getRandomSizeVariance(pixelVariance, 0.5);
      // shiftValue *= -1
      const newHSL = randomArrayItem(similarColors);
      ctx.fillStyle = getHSLStyle(newHSL);
      ctx.fillRect(x, y, sideLength + getRandomSizeVariance(pixelVariance, 0.5), sideLength + getRandomSizeVariance(pixelVariance, 0.5));
    }
  }
};

const drawCircleBoard = (ctx, similarColors, width, height, sideLength, pixelVariance, getRandomSizeVariance, randomArrayItem) => {
  let i, j, x, y;

  const hexHeight = 0.5 * sideLength;
  const circleRadius = 0.9 * sideLength;
  const circleDiameter = 2 * circleRadius;
  const offset = getRandomSizeVariance(sideLength * 1.3, 0);

  for (i = 0; i < width / sideLength + 1; ++i) {
    for (j = 0; j < height / sideLength + 1; ++j) {
      x = i * circleDiameter + ((j % 2) * circleRadius) - offset;
      y = j * (sideLength + hexHeight) - offset;
      ctx.beginPath();
      ctx.arc(x + sideLength, y + sideLength, sideLength + getRandomSizeVariance(pixelVariance, 0.9), 0, Math.PI * 2);
      const newHSL = randomArrayItem(similarColors);
      ctx.fillStyle = getHSLStyle(newHSL);
      ctx.fill();
    }
  }
};

const gridavatar = (seed, options) => {
  const opts = Object.assign({
    height: 256,
    width: 256,
    objSize: Math.min(14, (options?.width || 256) / 16),
    scatter: 0,
    luminosity: ['bright', 'light', 'dark'],
    text: false,
    textSize: 64,
    type: ['circle', 'square', 'hex'],
    output: 'image',
  }, options || {});

  // Fix user Errors that break things
  if (opts.scatter >= opts.objSize / 2) {
    opts.scatter = opts.objSize / 2;
  }

  if (typeof seed === 'string') {
    seed = cyrb128(seed)[0];
  }

  // Random number generator variations
  const randomGen = randomGenInit(seed); // Sets the seed for the twister; this is predictable
  const randomNumber = (minValue, maxValue) => {
    const max = maxValue || 1;
    const min = minValue || 0;

    const rnd = randomGen();

    return min + rnd * (max - min);
  };
  const randomArrayItem = items => items[Math.floor(randomGen() * items.length)];
  const getRandomSizeVariance = (b, v) => (randomNumber() * b) - (b * v);

  // Create the canvas
  const canvas = document.createElement('canvas');
  canvas.width = opts.width;
  canvas.height = opts.height;
  const ctx = canvas.getContext('2d');

  // Set our color space
  const starterHue = randomNumber(1, 334);
  const similarColors = randomColor({
    rand: randomNumber,
    hue: starterHue,
    format: 'hslArray',
    luminosity: randomArrayItem(opts.luminosity),
    count: 16,
  });
  // Background color
  const baseColor = similarColors.pop();

  ctx.fillStyle = getHSLStyle(baseColor);
  ctx.fillRect(0, 0, opts.width, opts.height);
  ctx.font = opts.textSize + 'px sans-serif';
  ctx.textAlign = 'center';

  const renderType = randomArrayItem(opts.type);

  switch (renderType) {
    case 'circle':
      drawCircleBoard(ctx, similarColors, opts.width, opts.height, opts.objSize, opts.scatter, getRandomSizeVariance, randomArrayItem);
      break;
    case 'square':
      drawSquareBoard(ctx, similarColors, opts.width, opts.height, opts.objSize, opts.scatter, getRandomSizeVariance, randomArrayItem);
      break;
    default:
      drawHexBoard(ctx, similarColors, opts.width, opts.height, opts.objSize, opts.scatter, getRandomSizeVariance, randomArrayItem);
      break;
  }

  if (opts.text) {
    ctx.fillStyle = getContrastColor(baseColor);
    ctx.fillText(opts.text, opts.width / 2, (opts.height / 2) + (opts.textSize / 3));
  }

  if (opts.output === 'dataURL') {
    return canvas.toDataURL('image/png');
  } else if (opts.output === 'canvas') {
    return canvas;
  } else {
    const image = new Image();
    image.src = canvas.toDataURL('image/png');
    return image;
  }
};

export default gridavatar;
