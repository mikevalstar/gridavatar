import { defineConfig } from 'vite';
import path from 'path';

export default defineConfig({
  build: {
    sourcemap: true,
    minify: 'esbuild',
    lib: {
      entry: path.resolve(__dirname, 'src/gridavatar.js'),
      name: 'gridavatar',
      filename: 'index',
    },
  },
});
